import React from 'react'
import {render} from 'react-dom'
import {Index} from "./components/Index";
import {Test} from "./components/Test";
import './stylesheets/style.scss';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";


window.React = React

// render(
//     <SkiDayCount total={"test"} /> ,
//     document.getElementById("react-container")
// )

render(
    <Router>
        <Switch>
            <Route exact path="/">
                <Index/>
            </Route>

            <Route path="/test">
                sdfgrtfhyujhgfdsffthyu
            </Route>

            <Route path="/test2/:id" component={Test} />

            <Route path="/*">
                404
            </Route>
        </Switch>
    </Router>,
    document.getElementById("react-container")
);
